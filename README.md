## Petcoach Back-end Guidelines

Welcome aboard, newcomer.

Here, you can find all required information for start your experience with our company and our team.

This repository provides information about the company, product, technologies involved and criteria adopted for develop awesome codes.

#### Our Guides

* [Petcoach project](/petcoach-project)
* [Symfony and Bundles](/symfony-and-bundles)

#### Contributing

This guide is maintained by back-end team from Oowlish. If you have any improvement, feel free for submit your pull-request.